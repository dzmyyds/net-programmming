import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class Main {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("127.0.0.1",1000);
        OutputStream OS = socket.getOutputStream();
        OS.write("hello,World".getBytes());


        OS.close();
        socket.close();

    }
}