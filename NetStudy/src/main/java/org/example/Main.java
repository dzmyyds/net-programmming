package org.example;

import java.net.InetAddress;

public class Main {
    public static void main(String[] args) throws Exception{
        InetAddress inetAddress = InetAddress.getLocalHost();

        System.out.println(inetAddress.getHostName()+"\n");
        System.out.println(inetAddress.getHostAddress()+"\n");
        InetAddress IP2 = InetAddress.getByName("www.baidu.com");
        System.out.println(IP2.getHostName()+"\n");
        System.out.println(IP2.getHostAddress()+"\n");

    }
}